(defsystem "vxsh"
	:description "shell experiment thing"
	:author "Valenoern"
	:licence "GPL"
	
	:depends-on (
		"grevillea"
		"xylem"
	)
	
	:components (
		(:file "session")  ; :vxsh.session
		(:file "sha")      ; :vxsh.sha
))
